import 'package:flutter/material.dart';

DateTime get startRatingDate {
  var now = DateTime.now();
  var sunday = now.add(Duration(days: -now.weekday - 7));
  return sunday;
}

DateTime get endRatingDate {
  var now = DateTime.now();
  var sunday = now.add(Duration(days: -now.weekday));
  return sunday;
}

showMessage(context, title, message) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(title),
        content: new Text(message.toString()),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Ok"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
