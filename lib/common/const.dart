class Const {
  static const String PLACES_API_KEY =
      'AIzaSyA1l_gcZ9OVV_qVq45pq0fJA8B5_ayGElo';

  static const String URL_TERMS_CONDITION =
      'https://drive.google.com/file/d/1WaUJhrhsvzB1T2MWZ5QNpHjIWKcW_rx1/view';
  static const String URL_PRIVACY_POLICY =
      'https://drive.google.com/file/d/1ONUNJ1v4qVvxt4-Smc5Jtm6yVRCj3YLR/view';
  static const String URL_HELP = 'https://www.zoomclass.info/help';

  static const String kPostDB = 'posts';
  static const String kActivitiesDB = 'post';
  static const String kUserDB = 'user';
  static const String kSubscriptionDB = 'subscription';
  static const String kRatingDB = 'rating';
  static const String kActivityTypesDB = 'activity';
  static const String kReportDB = 'report';
}
