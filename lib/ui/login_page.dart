import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/common/util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginPage extends StatelessWidget {
  final googleSignIn = new GoogleSignIn();

  final spanTextStyle = TextStyle(color: Colors.white, fontSize: 18);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Image.asset(
          'assets/images/background.jpg',
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
        Container(
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                colors: [Color(0x0F000000), Color(0xFF002A79)],
                begin: Alignment.bottomCenter,
                end: new Alignment(-1.0, -1.0)),
          ),
          child: SizedBox(
            height: 2000,
            width: 2000,
          ),
        ),
        Column(
          children: <Widget>[
            Container(
              child: Container(
                padding: EdgeInsets.fromLTRB(150.0, 50.0, 150.0, 0.0),
                child: Image.asset(
                  'assets/images/logo.png',
                ),
              ),
            ),
            Text(
              'Sports with friends',
              style: TextStyle(
                  color: Color(0xFFD1D1D1),
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(bottom: 100.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                GestureDetector(
                  onTap: () => _authenticateWithGoogle(context),
                  child: new Image.asset(
                    "assets/images/google_signin_button.png",
                    width: 225.0,
                  ),
                ),
              ],
            )),
        Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.only(bottom: 20.0),
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Using Zoomclass app, you accept\n',
                  style: spanTextStyle,
                ),
                TextSpan(
                  text: 'Terms&Condition',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.underline),
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () {
                      launch(Const.URL_TERMS_CONDITION);
                    },
                ),
                TextSpan(
                  text: ' and ',
                  style: spanTextStyle,
                ),
                TextSpan(
                  text: 'Privacy Policy.',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.underline),
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () {
                      launch(Const.URL_PRIVACY_POLICY);
                    },
                ),
              ],
            ),
          ),
        )
      ],
    ));
  }

  void _authenticateWithGoogle(BuildContext context) async {
    try {
      final user = await googleSignIn.signIn();
      final userAuth = await user.authentication;

      final userRecord = await Firestore.instance
          .collection(Const.kUserDB)
          .document(user.id)
          .get();

      if (userRecord.data == null) {
        final username = await Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (context) => new Center(
                    child: new Scaffold(
                        appBar: new AppBar(
                          leading: new Container(),
                          title: new Text('Fill out missing data',
                              style: new TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          backgroundColor: Colors.white,
                        ),
                        body: new ListView(
                          children: <Widget>[
                            new Container(
                              child: new CreateAccount(),
                            ),
                          ],
                        )),
                  )),
        );

        if (username == null || username.length == 0) {
          return;
        }

        await userRecord.reference.setData({
          "id": user.id,
          "photoUrl": user.photoUrl,
          "username": username,
          "email": user.email,
          "displayName": user.displayName,
          "description": "",
          "location": "",
          "rate": 0,
          "followers": {},
          "following": {},
          "created_at": FieldValue.serverTimestamp()
        });
      }

//    currentUserModel = new User.fromDocument(userRecord);

      await FirebaseAuth.instance.signInWithGoogle(
          accessToken: userAuth.accessToken, idToken: userAuth.idToken);
    } catch (e) {
      showMessage(context, '', 'An error has occurred.\nЕry again');
    }
  }
}

class CreateAccount extends StatefulWidget {
  @override
  _CreateAccountState createState() => new _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {
  final name = new TextEditingController();

  @override
  void dispose() {
    name.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Column(children: [
      new Padding(
        padding: const EdgeInsets.only(top: 25.0),
        child: new Center(
          child: new Text(
            "Create a username",
            style: new TextStyle(fontSize: 25.0),
          ),
        ),
      ),
      new Padding(
        padding: const EdgeInsets.all(16.0),
        child: new Container(
          decoration: new BoxDecoration(
              border: new Border.all(width: 1.0, color: Colors.black26),
              borderRadius: new BorderRadius.circular(7.0)),
          child: new TextField(
            controller: name,
            decoration: new InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.all(10.0),
                labelText: "Username",
                labelStyle: new TextStyle(fontSize: 15.0)),
          ),
        ),
      ),
      new GestureDetector(
          onTap: () {
            if (name.text == null || name.text.length == 0) {
              return;
            }
            Navigator.pop(context, name.text);
          },
          child: new Container(
            width: 350.0,
            height: 50.0,
            child: new Center(
                child: new Text(
              "Next",
              style: new TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold),
            )),
            decoration: new BoxDecoration(
                color: Colors.blue,
                borderRadius: new BorderRadius.circular(7.0)),
          ))
    ]);
  }
}
