import 'dart:io';
import 'dart:math' as Math;

import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/common/location.dart';
import 'package:Fluttergram/common/util.dart';
import 'package:Fluttergram/entity/activity.dart';
import 'package:Fluttergram/entity/user.dart';
import 'package:Fluttergram/repository/image_repository.dart'
    as ImageRepository;
import 'package:Fluttergram/repository/post_repository.dart' as PostRepository;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:image/image.dart' as Im;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class CreatePostScreen extends StatefulWidget {
  @override
  _CreatePostScreenState createState() => _CreatePostScreenState();
}

class _CreatePostScreenState extends State<CreatePostScreen> {
  TextEditingController typeController;
  TextEditingController locationController;
  TextEditingController descriptionController;

  Activity _choosedType;
  Map<String, double> _choosedLocation;
  File _file;

  String _typeIcon;

  var uploading = false;

  final homeScaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    typeController.dispose();
    locationController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    typeController = TextEditingController();
    locationController = TextEditingController();
    descriptionController = TextEditingController();
    super.initState();
  }

  _createPost() async {
    if (_choosedType == null) {
      showMessage(context, '', 'Choose activity');
      return;
    }
    setState(() {
      uploading = true;
    });

    try {
      var data;
      if (_file != null) {
        //compressImage();
        data = await ImageRepository.uploadImage(_file);
      }

      final user = await FirebaseAuth.instance.currentUser();
      final snapshot = await Firestore.instance
          .collection(Const.kUserDB)
          .document(user.providerData.last.uid)
          .snapshots()
          .first;
      final profile = User.fromDocument(snapshot);

      await PostRepository.createPost(
          imageUrl: data != null ? data['url'] : null,
          imagePath: data != null ? data['path'] : null,
          imageName: data != null ? data['name'] : null,
          ownerId: profile.id,
          ownerUsername: profile.username,
          ownerDisplayName: profile.fullName,
          ownerImage: profile.avatar,
          location: locationController.text,
          latitude: _choosedLocation != null ? _choosedLocation['lat'] : 0.0,
          longitude: _choosedLocation != null ? _choosedLocation['lon'] : 0.0,
          type: _choosedType.name,
          typeImage: _choosedType.icon,
          description: descriptionController.text,
          timestamp: FieldValue.serverTimestamp(),
          rating: _choosedType.rate);
      Navigator.pop(context, true);
    } catch (e) {
      showMessage(context, 'Error', e.toString());
    } finally {
      setState(() {
        uploading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
//        mainAxisSize: MainAxisSize.max,
//        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          uploading
              ? new LinearProgressIndicator()
              : new Padding(padding: new EdgeInsets.only(top: 0.0)),
          GestureDetector(
            onTap: () async {
              Activity type = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Scaffold(
                        appBar: AppBar(
                          brightness: Brightness.light,
                          title: Text('Share your activity',
                              style: TextStyle(color: Colors.black)),
                          backgroundColor: Color.fromRGBO(244, 244, 244, 1.0),
                        ),
                        body: TypeChooser())),
              );
              _choosedType = type;
              typeController.value = TextEditingValue(text: type.name);
              setState(() {
                _typeIcon = type.icon;
              });
            },
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 1.0, color: Colors.black26),
                    borderRadius: BorderRadius.circular(7.0)),
                child: TextField(
                  enabled: false,
                  controller: typeController,
                  decoration: InputDecoration(
                      suffixIcon: _typeIcon == null
                          ? Icon(Icons.settings)
                          : Container(
                              padding: EdgeInsets.all(12.0),
                              child: SvgPicture.network(
                                _typeIcon,
                                color: Colors.blue,
                                width: 16.0,
                                height: 16.0,
                              ),
                            ),
                      border: InputBorder.none,
                      contentPadding: const EdgeInsets.all(10.0),
                      hintText: "Choose activity type",
                      hintStyle: TextStyle(fontSize: 15.0)),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: _selectImage,
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _file == null
                      ? SvgPicture.asset(
                          'assets/images/photo.svg',
                          width: 56.0,
                          height: 56.0,
                          color: Colors.blue,
                        )
                      : Stack(
                          alignment: AlignmentDirectional.bottomEnd,
                          children: <Widget>[
                            Image.file(
                              _file,
                              width: 72.0,
                              height: 72.0,
                            ),
                            Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24.0),
                              ),
                              elevation: 4.0,
                              child: Icon(
                                Icons.check_circle,
                                color: Colors.blue,
                              ),
                            )
                          ],
                        )
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () async {
              var location;
              try {
                var loc = await getUserLocation();
                location = {'lat': loc['latitude'], 'lon': loc['longitude']};
              } catch (e) {
                location = {'lat': 0.0, 'lon': 0.0};
              }
              Prediction p = await PlacesAutocomplete.show(
                  context: context,
                  apiKey: Const.PLACES_API_KEY,
                  onError: (res) {
                    print(res.errorMessage);
                  },
                  location: Location(location['lat'], location['lon']),
                  radius: 50000,
                  mode: Mode.overlay,
                  language: "en");
              var _places = new GoogleMapsPlaces(apiKey: Const.PLACES_API_KEY);
              PlacesDetailsResponse detail =
                  await _places.getDetailsByPlaceId(p.placeId);
              final lat = detail.result.geometry.location.lat;
              final lng = detail.result.geometry.location.lng;
              _choosedLocation = {'lat': lat, 'lon': lng};
              locationController.value = TextEditingValue(text: p.description);
            },
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 1.0, color: Colors.black26),
                    borderRadius: BorderRadius.circular(7.0)),
                child: TextField(
                  enabled: false,
                  controller: locationController,
                  decoration: InputDecoration(
                      suffixIcon: Icon(Icons.place, color: Color(0xFFEE3840)),
                      border: InputBorder.none,
                      contentPadding: const EdgeInsets.all(10.0),
                      hintText: "Choose location",
                      hintStyle: TextStyle(fontSize: 15.0)),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: Colors.black26),
                  borderRadius: BorderRadius.circular(7.0)),
              child: TextField(
                keyboardType: TextInputType.multiline,
                maxLines: 4,
                enabled: !uploading,
                textInputAction: TextInputAction.done,
                controller: descriptionController,
                onSubmitted: (_) => _createPost(),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: "Describe your activity",
                  hintStyle: TextStyle(fontSize: 15.0),
                ),
              ),
            ),
          ),
          uploading
              ? SizedBox()
              : Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: GestureDetector(
                    onTap: _createPost,
                    child: Container(
                      width: 250.0,
                      height: 50.0,
                      child: Center(
                        child: Text(
                          "Done",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(7.0),
                      ),
                    ),
                  ),
                )
        ]);
  }

  void compressImage() async {
    print('starting compression');
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    int rand = new Math.Random().nextInt(10000);

    Im.Image image = Im.decodeImage(_file.readAsBytesSync());
    Im.copyResize(image, 500);

//    image.format = Im.Image.RGBA;
//    Im.Image newim = Im.remapColors(image, alpha: Im.LUMINANCE);

    var newim2 = new File('$path/img_$rand.jpg')
      ..writeAsBytesSync(Im.encodeJpg(image, quality: 85));

    setState(() {
      _file = newim2;
    });
    print('done');
  }

  _selectImage() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new SimpleDialog(
          title: const Text('Image'),
          children: <Widget>[
            new SimpleDialogOption(
                child: const Text('Take a photo'),
                onPressed: () async {
                  Navigator.pop(context);
                  File imageFile =
                      await ImagePicker.pickImage(source: ImageSource.camera);
                  setState(() {
                    _file = imageFile;
                  });
                }),
            new SimpleDialogOption(
                child: const Text('Choose from Gallery'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  File imageFile =
                      await ImagePicker.pickImage(source: ImageSource.gallery);
                  setState(() {
                    _file = imageFile;
                  });
                }),
            new SimpleDialogOption(
              child: const Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }
}

class TypeChooser extends StatefulWidget {
  @override
  _TypeChooserState createState() => _TypeChooserState();
}

class _TypeChooserState extends State<TypeChooser> {
  final scrollController = ScrollController();
  TextEditingController searchController;

  List<Activity> types = List();
  List<Activity> searchTypes = List();

  @override
  void initState() {
    searchController = TextEditingController();
    super.initState();
    searchController.addListener(() {
      setState(() {
        if (searchController.text.isEmpty) {
          searchTypes = types;
          return;
        }
        searchTypes = types
            .where((type) =>
                type.name.toLowerCase().contains(searchController.text))
            .toList();
      });
    });
    fetchActivityTypes.listen((snap) {
      types.clear();
      snap.documents.forEach((doc) => types.add(Activity.fromDocument(doc)));
      setState(() {
        searchTypes = types;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        getSearchView(),
        Expanded(
          child: ListView.builder(
            itemCount: searchTypes.length,
            itemBuilder: (ctx, p) {
              return GestureDetector(
                onTap: () => Navigator.pop(context, searchTypes[p]),
                child: Container(
                  padding: EdgeInsets.fromLTRB(24.0, 16.0, 24.0, 16.0),
                  child: Row(
                    children: <Widget>[
                      SvgPicture.network(
                        searchTypes[p].icon ?? '',
                        color: Colors.blue,
                        width: 24.0,
                        height: 24.0,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16.0),
                        child: Text(
                            toBeginningOfSentenceCase(searchTypes[p].name),
                            style: TextStyle(fontSize: 16.0)),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }

  Widget getSearchView() {
    return Card(
      margin: EdgeInsets.all(16.0),
      shape: RoundedRectangleBorder(
        side: BorderSide(width: 1.0, color: Color(0xFFD1D1D1)),
        borderRadius: BorderRadius.circular(5.0),
      ),
      elevation: 1.0,
      child: TextField(
          enabled: true,
          controller: searchController,
          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.all(16.0),
              hintText: "Search",
              hintStyle: TextStyle(fontSize: 15.0)),
          style: TextStyle(fontSize: 15.0, color: Colors.blue)),
    );
  }

  Stream<QuerySnapshot> get fetchActivityTypes async* {
    final user = await FirebaseAuth.instance.currentUser();
    yield* Firestore.instance.collection(Const.kActivityTypesDB).snapshots();
  }
}
