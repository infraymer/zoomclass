import 'package:Fluttergram/entity/user.dart';
import 'package:Fluttergram/repository/user_repository.dart' as UserRepository;
import 'package:flutter/material.dart';

class EditProfileScreen extends StatefulWidget {
  final User user;

  EditProfileScreen(this.user);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  var usernameController = TextEditingController();
  var displayNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    usernameController.text = widget.user.username;
    displayNameController.text = widget.user.fullName;
  }

  @override
  void dispose() {
    usernameController.dispose();
    displayNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Edit profile',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold))),
      body: ListView(
        children: <Widget>[
          _buildUserNameTextField(),
          _buildDisplayNameTextField(),
          _buildSaveButton()
        ],
      ),
    );
  }

  _onTapSaveButton() {
    UserRepository.updateProfile(
      widget.user.id,
      usernameController.text,
      displayNameController.text,
    );
    Navigator.pop(
      context,
      [
        usernameController.text,
        displayNameController.text,
      ],
    );
  }

  _buildUserNameTextField() => Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(4),
              alignment: Alignment.centerLeft,
              child: Text('Username'),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: Colors.black26),
                  borderRadius: BorderRadius.circular(7.0)),
              child: TextField(
                keyboardType: TextInputType.multiline,
                maxLines: 1,
                textInputAction: TextInputAction.done,
                controller: usernameController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: "Username",
                  hintStyle: TextStyle(fontSize: 15.0),
                ),
              ),
            ),
          ],
        ),
      );

  _buildDisplayNameTextField() => Padding(
        padding: const EdgeInsets.only(left: 16, right: 16),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(4),
              alignment: Alignment.centerLeft,
              child: Text('Full name'),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: Colors.black26),
                  borderRadius: BorderRadius.circular(7.0)),
              child: TextField(
                keyboardType: TextInputType.multiline,
                maxLines: 1,
                textInputAction: TextInputAction.done,
                controller: displayNameController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: "Full name",
                  hintStyle: TextStyle(fontSize: 15.0),
                ),
              ),
            ),
          ],
        ),
      );

  _buildSaveButton() => Padding(
        padding: const EdgeInsets.all(16.0),
        child: GestureDetector(
          onTap: _onTapSaveButton,
          child: Container(
            width: 250.0,
            height: 50.0,
            child: Center(
              child: Text(
                "Save",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(7.0),
            ),
          ),
        ),
      );
}
