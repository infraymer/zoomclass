import 'package:Fluttergram/repository/user_repository.dart' as UserRepository;
import 'package:Fluttergram/ui/login_page.dart';
import 'package:Fluttergram/ui/main_screen/main_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<FirebaseUser>(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return splash();
          } else {
            if (snapshot.hasData) {
              UserRepository.profileId = snapshot.data.providerData.last.uid;
              return MainPage();
            }
            return LoginPage();
          }
        });
  }

  Widget splash() {
    return Container();
  }
}
