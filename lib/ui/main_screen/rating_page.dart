import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/entity/rating.dart';
import 'package:Fluttergram/entity/user.dart';
import 'package:Fluttergram/repository/rating_repository.dart'
    as RatingRepository;
import 'package:Fluttergram/ui/profile_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RatingPage extends StatefulWidget {
  @override
  State<RatingPage> createState() => new _RatingPageState();
}

class _RatingPageState extends State<RatingPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<RatingFriend>>(
      future: fetchRating,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _buildList(context, snapshot.data);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner
        return Container(
          height: 400.0,
          child: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }

  Future<List<RatingFriend>> get fetchRating async {
    final user = await FirebaseAuth.instance.currentUser();
    final profileDoc = await Firestore.instance
        .collection(Const.kUserDB)
        .document(user.providerData.last.uid)
        .get();
    final profile = User.fromDocument(profileDoc);
    return await RatingRepository.fetchRating(profile.id);
  }

  Widget _buildList(BuildContext context, List<RatingFriend> users) {
    return ListView.builder(
      padding: const EdgeInsets.only(top: 10.0),
      itemCount: users.length,
      itemBuilder: (context, position) {
        return RatingView(
          item: users[position],
          position: position,
        );
      },
    );
  }
}

class RatingView extends StatelessWidget {
  final RatingFriend item;
  final int position;

  const RatingView({Key key, this.item, this.position}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => openProfile(context, item.id),
        child: Container(
          margin: EdgeInsets.fromLTRB(3, 5, 3, 10),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 13.0,
              offset: Offset(0.0, 10.0),
              // spreadRadius: 10.0
            )
          ]),
          child: Stack(
            alignment: AlignmentDirectional.bottomStart,
            children: <Widget>[
              Card(
                  margin: EdgeInsets.fromLTRB(16.0, 4.0, 16.0, 4.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0),
                  ),
                  elevation: 0.0,
                  child: Container(
                      margin: EdgeInsets.all(8.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                CircleAvatar(
                                  radius: 36.0,
                                  backgroundColor: Colors.grey,
                                  backgroundImage: NetworkImage(item.avatar),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 16.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          '@' + item.username,
                                          style: TextStyle(
                                              color: Color(0xFF707070),
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    item.sumRating.toString(),
                                    style: TextStyle(
                                        color: Color(0xFF555555),
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Arial Rounded MT Bold'),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 4.0),
                                    child: Icon(
                                      Icons.star,
                                      color: Color(0xFFFAC917),
                                      size: 24.0,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ]))),
              Container(
                //width: 50.0,
                //height: 50.0,
                margin: EdgeInsets.only(left: 24.0, bottom: 4.0),
                padding: const EdgeInsets.all(8.0),
                //I used some padding without fixed width and height
                decoration: new BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                        color: Colors.black38,
                        offset: new Offset(1.0, 1.0),
                        blurRadius: 4.0)
                  ],
                  shape: BoxShape.circle,
                  // You can use like this way or like the below line
                  //borderRadius: new BorderRadius.circular(30.0),
                  color: Colors.white,
                ),
                child: new Text((position + 1).toString(),
                    style: new TextStyle(
                        color: Color(0xFFACACAC),
                        fontSize:
                            20.0)), // You can add a Icon instead of text also, like below.
                //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
              ),
            ],
          ),
        ));
  }
}
