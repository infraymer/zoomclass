import 'dart:async';

import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/entity/user.dart';
import 'package:Fluttergram/repository/user_repository.dart' as UserRepository;
import 'package:Fluttergram/ui/profile_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class FriendsPage extends StatefulWidget {
  @override
  State<FriendsPage> createState() => new _FriendsPageState();
}

User me;

class _FriendsPageState extends State<FriendsPage> {
  TextEditingController searchController;

  bool loading = true;

  List<User> friends = List();
  List<User> list = List();

  Timer _debounce;

  bool isFocus = false;

  @override
  void dispose() {
    searchController.removeListener(_onSearchChanged);
    searchController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    searchController = TextEditingController();
    searchController.addListener(_onSearchChanged);
    _focusNode.addListener(() {
      setState(() {
        isFocus = _focusNode.hasFocus;
      });
    });
    super.initState();
    loading = true;
    fetchFriends().then((usrs) {
      setState(() {
        loading = false;
        friends = usrs;
        list.addAll(friends);
      });
    });
  }

  _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
//      _handleChangeSearch();
      setState(() {});
    });
  }

  _handleChangeSearch() {
    if (searchController.text.isEmpty) {
      list.clear();
      list.addAll(friends);
      setState(() {});
      return;
    }

    var query = searchController.text.toLowerCase();
    var slist = friends
        .where((u) =>
            u.username.toLowerCase().contains(query) ||
            u.fullName.toLowerCase().contains(query))
        .toList();
    list.clear();
    slist.forEach((u) => list.add(u));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        loading
            ? new LinearProgressIndicator()
            : new Padding(padding: new EdgeInsets.only(top: 0.0)),
        getSearchView(),
        Expanded(
            child: list.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    itemCount: list.length,
                    itemBuilder: (context, position) {
                      return searchController.text.isEmpty
                          ? FriendsView(item: list[position], me: me)
                          : list[position].username.toLowerCase().contains(
                                      searchController.text.toLowerCase()) ||
                                  list[position]
                                      .fullName
                                      .toLowerCase()
                                      .contains(
                                          searchController.text.toLowerCase())
                              ? FriendsView(item: list[position], me: me)
                              : SizedBox();
                    },
                  )
                : Container(
                    margin: EdgeInsets.all(24),
                    alignment: Alignment.center,
                    child: Text(
                      'Add friends to see their sport activities here.',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black45),
                    ),
                  ))
      ],
    );
  }

  Future<List<User>> fetchFriends() async {
    final user = await FirebaseAuth.instance.currentUser();

    final profileDoc = await Firestore.instance
        .collection(Const.kUserDB)
        .document(user.providerData.last.uid)
        .get();
    final profile = User.fromDocument(profileDoc);
    me = profile;

    return await UserRepository.fetchFriends(profile.id);
  }

  Future<List<User>> searchUser(String query) async {
    return await UserRepository.searchUser(query);
  }

  var _focusNode = new FocusNode();

  Widget getSearchView() {
    return Card(
        margin: EdgeInsets.all(16.0),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1.0, color: Color(0xFFD1D1D1)),
          borderRadius: BorderRadius.circular(5.0),
        ),
        elevation: 1.0,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            TextField(
                focusNode: _focusNode,
                enabled: true,
                controller: searchController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: const EdgeInsets.all(16.0),
                    hintText: "Search",
                    hintStyle: TextStyle(fontSize: 15.0)),
                style: TextStyle(fontSize: 15.0, color: Colors.blue)),
            isFocus
                ? Container(
                    margin: EdgeInsets.only(right: 8),
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      onTap: () {
                        searchController.clear();
                        FocusScope.of(context).requestFocus(new FocusNode());
                        _handleChangeSearch();
                      },
                      child: Icon(Icons.clear),
                    ),
                  )
                : SizedBox()
          ],
        ));
  }

  Stream<QuerySnapshot> get fetchUsers async* {
    final user = await FirebaseAuth.instance.currentUser();
    yield* Firestore.instance
        .collection(Const.kUserDB)
        .where('username', isEqualTo: searchController.text)
        .snapshots();
  }

  Future<List<User>> _getUsers() async {
    final snap = await Firestore.instance
        .collection(Const.kUserDB)
        .limit(30)
        .getDocuments();
    final us = List<User>();
    snap.documents.forEach((doc) {
      us.add(User.fromDocument(doc));
    });
    return us;
  }
}

class FriendsView extends StatefulWidget {
  final User item;
  final User me;
  final onFollowTap;

  const FriendsView({Key key, this.item, this.me, this.onFollowTap})
      : super(key: key);

  @override
  _FriendsViewState createState() => _FriendsViewState(me, item);
}

class _FriendsViewState extends State<FriendsView> {
  User me;
  User item;

  bool _subscribed = false;

  _FriendsViewState(this.me, this.item);

  @override
  void initState() {
    _subscribed = me.following.containsKey(item.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => openProfile(context, item.id),
        child: Container(
          margin: EdgeInsets.fromLTRB(3, 4, 3, 4),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 13.0,
              offset: Offset(0.0, 10.0),
              // spreadRadius: 10.0
            )
          ]),
          child: Card(
              margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0),
              ),
              elevation: 0,
              child: Container(
                  margin: EdgeInsets.all(8.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 24.0,
                          backgroundColor: Colors.grey,
                          backgroundImage: NetworkImage(item.avatar),
                        ),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 16, right: 8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    '@' + item.username,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Color(0xFF707070),
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    item.fullName,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                      color: Colors.black45,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        _subscribed ? unfollowButton : followButton
                      ]))),
        ));
  }

  Widget get unfollowButton => GestureDetector(
        onTap: () => subscribe(false),
        child: Container(
          child: Card(
            margin: EdgeInsets.only(right: 8.0),
            elevation: 1,
            color: Color(0xFFF4F4F4),
            child: Padding(
              padding: EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
              child: Text(
                'Unfollow',
                style: TextStyle(color: Colors.black54, fontSize: 12.0),
              ),
            ),
          ),
        ),
      );

  Widget get followButton => GestureDetector(
        onTap: () => subscribe(true),
        child: Container(
          child: Card(
            margin: EdgeInsets.only(right: 8.0),
            elevation: 4,
            color: Colors.lightBlue,
            child: Padding(
              padding: EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
              child: Text(
                'Follow',
                style: TextStyle(color: Colors.white, fontSize: 12.0),
              ),
            ),
          ),
        ),
      );

  Future<void> subscribe(bool subscribe) async {
    setState(() {
      _subscribed = subscribe;
    });
//    widget.onFollowTap(item, subscribe);
    if (subscribe)
      widget.me.following[item.id] = DateTime.now();
    else
      widget.me.following.remove(item.id);
    await UserRepository.subscribeOnUser(me.id, item.id, subscribe);
  }
}
