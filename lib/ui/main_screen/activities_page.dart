import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/common/util.dart';
import 'package:Fluttergram/entity/post.dart';
import 'package:Fluttergram/entity/user.dart';
import 'package:Fluttergram/repository/post_repository.dart' as PostRepository;
import 'package:Fluttergram/ui/profile_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view.dart';

User meUser;

class ActivitiesPage extends StatefulWidget {
  @override
  State<ActivitiesPage> createState() => new _ActivitiesPageState();
}

class _ActivitiesPageState extends State<ActivitiesPage> {
  var _posts = List<Post>();
  var _isProgress = false;

  @override
  void initState() {
    super.initState();
    Firestore.instance
        .collection('post')
        .where("timestamp", isGreaterThanOrEqualTo: DateTime.now())
        .snapshots()
        .listen((data) => data.documents.forEach((doc) {
              var post = Post.fromDocument(doc);
              if (meUser.following.containsKey(post.ownerId) ||
                  post.ownerId == meUser.id)
                setState(() {
                  _posts.insert(0, Post.fromDocument(doc));
                });
            }));
    _isProgress = true;
    fetchPosts.then((data) {
      setState(() {
//        getUrl(data[0].imageUrl);
        _isProgress = false;
        _posts = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isProgress
        ? Center(child: CircularProgressIndicator())
        : _posts.isNotEmpty
            ? ListView(
                children: _posts
                    .map((post) => ActivitiesView(
                          item: post,
                          isMe: meUser.id == post.ownerId,
                          onDelete: () {
                            deletePost(post).then((some) {});
                          },
                          onReport: () => _onReportClicked(post),
                        ))
                    .toList(),
              )
            : Container(
                margin: EdgeInsets.all(24),
                alignment: Alignment.center,
                child: Text(
                  'Add your first sport activity',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black45),
                ),
              );
  }

  Future<List<Post>> get fetchPosts async {
    final user = await FirebaseAuth.instance.currentUser();

    final profileDoc = await Firestore.instance
        .collection(Const.kUserDB)
        .document(user.providerData.last.uid)
        .get();
    final profile = User.fromDocument(profileDoc);
    meUser = profile;

    return await PostRepository.fetchPosts(profile.id);
  }

  /*Stream get fetchPost async* {
    final profileDoc = await Firestore.instance
        .collection(Const.kUserDB)
        .document(user.providerData.last.uid)
        .get();
    final profile = User.fromDocument(profileDoc);
    meUser = profile;
  }*/

  Future<void> deletePost(Post post) async {
    try {
      await Firestore.instance.document('post/${post.id}').delete();
      setState(() {
        _posts.remove(post);
      });
      showMessage(context, 'Success!', 'Your post deleted');
    } catch (e) {
      showMessage(context, 'Error!', e.toString());
    }
  }

  reportPost(Post post, String name) async {
    var act = {
      'ownerId': meUser.id,
      'post': post.id,
      'description': name,
      'timestamp': FieldValue.serverTimestamp(),
    };
    await Firestore.instance
        .collection(Const.kReportDB)
        .document()
        .setData(act);
    setState(() {
      showMessage(context, 'Thank you for your report',
          'Our team will review it in 24 hours.');
    });
  }

  List<String> _reports = [
    'Low-quality image quality',
    'Spam or fraud',
    'Nakedness',
    'Harassment',
    'Illegal information',
  ];

  _onReportClicked(Post post) {
    showModalBottomSheet<void>(
        context: this.context,
        builder: (BuildContext context) {
          return new Column(
              mainAxisSize: MainAxisSize.min,
              children: _reports.map((name) {
                return ListTile(
                  onTap: () {
                    reportPost(post, name);
                    Navigator.pop(context);
                  },
                  leading: SizedBox(),
                  title: Text(name),
                );
              }).toList());
        });
  }
}

class ActivitiesView extends StatelessWidget {
  final Post item;
  final bool isMe;

  VoidCallback onDelete;
  VoidCallback onReport;

  ActivitiesView({Key key, this.item, this.isMe, this.onDelete, this.onReport})
      : super(key: key);

  List<String> _menuItems = [];

  _onMenuItemSelected(String name) {
    switch (name) {
      case 'Report':
        onReport();
        break;
      case 'Delete':
        onDelete();
        break;
    }
  }

  showMenuDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
            content: Column(
          mainAxisSize: MainAxisSize.min,
          children: _menuItems
              .map((name) => ListTile(
                    title: Text(name),
                    onTap: () {
                      Navigator.pop(context);
                      _onMenuItemSelected(name);
                    },
                  ))
              .toList(),
        ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (isMe)
      _menuItems = ['Delete'];
    else
      _menuItems = ['Report'];

    var url = getUrl(item.imageUrl);

    return Container(
      margin: EdgeInsets.fromLTRB(3, 5, 3, 10),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.black12,
          blurRadius: 13.0,
          offset: Offset(0.0, 10.0),
          // spreadRadius: 10.0
        )
      ]),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        // margin: EdgeInsets.all(30.0),
        elevation: 0.0,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: new Column(children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () => openProfile(context, item.ownerId),
                  child: CircleAvatar(
                    radius: 24.0,
                    backgroundColor: Colors.grey,
                    backgroundImage: NetworkImage(item.ownerImage),
                  ),
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        // NAME
                        Container(
                          margin: EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
                          child: Text(
                            item.ownerFullname,
                            style: TextStyle(
                                color: Color(0xFF555555),
                                fontSize: 13.0,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        // ICON TYPE
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                alignment: Alignment.bottomCenter,
                                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                child: item.typeImage != null
                                    ? SvgPicture.network(
                                        item.typeImage,
                                        width: 20.0,
                                        height: 20.0,
                                        color: Colors.blue,
                                      )
                                    : SizedBox(
                                        width: 20.0,
                                        height: 20.0,
                                      )),
                            GestureDetector(
                              onTap: () => showMenuDialog(context),
                              child: Icon(Icons.more_vert),
                            )
                          ],
                        )
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.fromLTRB(8.0, 8.0, 2.0, 8.0),
                          child: Text(
                              DateFormat('MMM, d').format(item.timestamp),
                              style: TextStyle(
                                  color: Colors.black45,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 11.0)),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(8.0, 8.0, 2.0, 8.0),
                          child: Text(
                            toBeginningOfSentenceCase(item.type),
                            style: TextStyle(
                              color: Color(0xFF555555),
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ))
              ],
            ),
            item.imageUrl == null
                ? SizedBox()
                : GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Scaffold(
                                  appBar: AppBar(
                                    backgroundColor:
                                        Color.fromRGBO(244, 244, 244, 1.0),
                                    title: Text('Photo',
                                        style: TextStyle(color: Colors.black)),
                                    brightness: Brightness.light,
                                  ),
                                  body: Container(
                                    child: PhotoView(
                                      imageProvider:
                                          NetworkImage(item.imageUrl),
                                    ),
                                  ))));
                    },
                    child: Container(
                        height: 250,
                        width: double.infinity,
                        child: ClipRect(
                          child: CachedNetworkImage(
                            alignment: Alignment.center,
                            fit: BoxFit.cover,
                            height: 250,
                            imageUrl: item.imageUrl,
                            placeholder: Container(
                              color: Colors.black12,
                            ),
                          ),
                        )),
                  ),
            item.description != null
                ? Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.fromLTRB(0.0, 8.0, 2.0, 0.0),
                    child: Text(
                      (item.description.length ?? 0) > 200
                          ? item.description.substring(0, 200) + '...'
                          : item.description,
                      maxLines: 5,
                      style: TextStyle(
                          fontStyle: FontStyle.italic, fontSize: 14.0),
                    ),
                  )
                : SizedBox(),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: item.location.isEmpty
                      ? SizedBox()
                      : Row(
                          children: <Widget>[
                            Icon(
                              Icons.place,
                              color: Colors.red,
                              size: 14.0,
                            ),
                            Container(
                              width: 200,
                              padding: EdgeInsets.only(left: 4.0, right: 8.0),
                              child: Text(
                                item.location,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.black38,
                                ),
                              ),
                            )
                          ],
                        ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      item.rating.toString(),
                      style: TextStyle(
                          color: Color(0xFF555555),
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Arial Rounded MT Bold'),
                    ),
                    Icon(
                      Icons.star,
                      color: Color(0xFFFAC917),
                      size: 24.0,
                    )
                  ],
                ),
              ],
            )
          ]),
        ),
      ),
    );
  }

  String getUrl(String url) {
    try {
//      var decode = Uri.decodeFull(url);
//      var i = decode.lastIndexOf('/');
//      var res = decode.replaceRange(i, i + 1, '/thumb@512_');
//      return Uri.encodeFull(res);
      var i = url.lastIndexOf('%2F');
      var res = url.replaceRange(i, i + 3, '%2Fthumb@512_');
      return res;
    } catch (e) {
      return null;
    }
  }
}
