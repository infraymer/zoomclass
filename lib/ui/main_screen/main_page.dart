import 'package:Fluttergram/common/util.dart';
import 'package:Fluttergram/ui/create_post_screen.dart';
import 'package:Fluttergram/ui/main_screen/activities_page.dart';
import 'package:Fluttergram/ui/main_screen/friends_page.dart';
import 'package:Fluttergram/ui/main_screen/rating_page.dart';
import 'package:Fluttergram/ui/main_screen/search_page.dart';
import 'package:Fluttergram/ui/profile_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MainPage extends StatefulWidget {
  @override
  State<MainPage> createState() => _MainPagState();
}

class _MainPagState extends State<MainPage> {
  PageController pageController;
  int _page = 0;

  final titles = [
    'Friends activities',
    'Weekly rating',
    'Search users',
    'My friends',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          _page != 1
              ? SizedBox()
              : Container(
                  margin: EdgeInsets.only(right: 8),
                  child: IconButton(
                      icon: Icon(Icons.info_outline),
                      onPressed: () {
                        showMessage(context, '',
                            'Raiting updates weekly on sunday 12 AM UTC');
                      }),
                ),
          ProfileAvatar()
        ],
        brightness: Brightness.light,
        leading: Container(
          padding: EdgeInsets.all(10.0),
          child: SvgPicture.asset(
            'assets/images/zc_icon.svg',
            color: Colors.blue,
          ),
        ),
        title: Text(titles[_page], style: TextStyle(color: Colors.black)),
        backgroundColor: Color.fromRGBO(244, 244, 244, 1.0),
      ),
      body: PageView(
        children: [
          Container(color: Colors.white, child: ActivitiesPage()),
          Container(color: Colors.white, child: RatingPage()),
          Container(color: Colors.white, child: SearchPage()),
          Container(color: Colors.white, child: FriendsPage()),
        ],
        controller: pageController,
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: onPageChanged,
      ),
      bottomNavigationBar: CupertinoTabBar(
        activeColor: Colors.blue,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.list,
                  color: (_page == 0) ? Colors.blue : Colors.grey),
              title: Text("Activities"),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
              icon: Icon(Icons.local_activity,
                  color: (_page == 1) ? Colors.blue : Colors.grey),
              title: Text("Rating"),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
              icon: Icon(Icons.search,
                  color: (_page == 2) ? Colors.blue : Colors.grey),
              title: Text("Search"),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
              icon: Icon(Icons.people,
                  color: (_page == 3) ? Colors.blue : Colors.grey),
              title: Text("Friends"),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
              icon: Icon(Icons.add_circle_outline,
                  color: (_page == 4) ? Colors.blue : Colors.grey),
              title: Text("Add new"),
              backgroundColor: Colors.white)
        ],
        onTap: navigationTapped,
        currentIndex: _page,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: _page);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  void navigationTapped(int page) async {
    if (page == 4) {
      var isPosted = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Scaffold(
                appBar: AppBar(
                  brightness: Brightness.light,
                  title: Text('Share your activity',
                      style: TextStyle(color: Colors.black)),
                  backgroundColor: Color.fromRGBO(244, 244, 244, 1.0),
                ),
                body: CreatePostScreen())),
      );
      if (isPosted)
        setState(() {
          pageController.jumpToPage(0);
        });
      return;
    }

    pageController.jumpToPage(page);
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}

class ProfileAvatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: fetchActivities,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());

        return GestureDetector(
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => SelfProfileUser())),
          child: new Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundImage: new NetworkImage(snapshot.data),
            ),
          ),
        );
      },
    );
  }

  Stream<String> get fetchActivities async* {
    final user = await FirebaseAuth.instance.currentUser();
    yield user.photoUrl;
  }
}
