import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/common/util.dart';
import 'package:Fluttergram/entity/post.dart';
import 'package:Fluttergram/entity/user.dart';
import 'package:Fluttergram/repository/post_repository.dart' as PostRepository;
import 'package:Fluttergram/repository/user_repository.dart' as UserRepository;
import 'package:Fluttergram/ui/edit_profile_screen.dart';
import 'package:Fluttergram/ui/welcome_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

User meProfile;

class ProfileUser extends StatefulWidget {
  final User item;

  ProfileUser({Key key, this.item}) : super(key: key);

  @override
  _ProfileUserState createState() => _ProfileUserState();
}

class _ProfileUserState extends State<ProfileUser> {
  var _isMe = false;
  bool _isSubscribed = false;

  @override
  void initState() {
    _menuItems = meProfile.id == widget.item.id
        ? [
            'Edit profile',
            'Terms&Condition',
            'Privacy Policy',
            'Help',
            'Log out'
          ]
        : ['Report'];
    _isMe = meProfile.id == widget.item.id;
    _isSubscribed = meProfile.following.containsKey(widget.item.id);
    super.initState();
  }

  Future<bool> get isMe async {
    final user = await FirebaseAuth.instance.currentUser();
    return user.providerData.last.uid == widget.item.id;
  }

  Widget getHeader(BuildContext ctx, int rate) {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                CircleAvatar(
                  radius: 56.0,
                  backgroundColor: Colors.grey,
                  backgroundImage: NetworkImage(widget.item.avatar),
                ),
                Container(
                  margin: EdgeInsets.only(top: 70.0, left: 70.0),
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      SvgPicture.asset('assets/images/star.svg'),
                      Text(
                        rate.toString(),
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 8.0),
              child: Text(
                widget.item.fullName,
                style: TextStyle(color: Color(0xFF707070), fontSize: 18.0),
              ),
            ),
            Text(widget.item.location),
            Text(widget.item.description),
            meProfile.id == widget.item.id ? SizedBox() : followButton()
          ],
        ));
  }

  Widget followButton() {
    return _isSubscribed
        ? GestureDetector(
            onTap: () async {
              setState(() {
                _isSubscribed = false;
              });
              await subscribe(false);
            },
            child: Container(
              child: Card(
                margin: EdgeInsets.only(right: 8.0),
                elevation: 1,
                color: Color(0xFFF4F4F4),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                  child: Text(
                    'Unfollow',
                    style: TextStyle(color: Colors.black54, fontSize: 18.0),
                  ),
                ),
              ),
            ),
          )
        : GestureDetector(
            onTap: () async {
              setState(() {
                _isSubscribed = true;
              });
              await subscribe(true);
            },
            child: Container(
              child: Card(
                margin: EdgeInsets.only(right: 8.0),
                elevation: 4,
                color: Colors.lightBlue,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                  child: Text(
                    'Follow',
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                ),
              ),
            ),
          );
  }

  Future<void> subscribe(bool subscribe) async {
    await UserRepository.subscribeOnUser(
        meProfile.id, widget.item.id, subscribe);
  }

  swapSubscription() {}

  Future<Null> _logOut() async {
    try {
      final googleSignIn = new GoogleSignIn();
      await FirebaseAuth.instance.signOut();
      await googleSignIn.signOut();

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (ctx) => WelcomePage()));
    } catch (e) {
      showMessage(context, 'Error', 'Try again');
    }
  }

  List<String> _menuItems;

  /* List<String> _reports = [
    'Low-quality image quality',
    'Spam or fraud',
    'Nakedness',
    'Harassment',
    'Illegal information',
  ];*/

  List<String> _reports = ['Report'];

  _onMenuItemSelectedMe(String name) {
    var i = _menuItems.indexOf(name);
    switch (i) {
      case 0:
        _onEditProfileClicked();
        break;
      case 1:
        launch(Const.URL_TERMS_CONDITION);
        break;
      case 2:
        launch(Const.URL_PRIVACY_POLICY);
        break;
      case 3:
        launch(Const.URL_HELP);
        break;
      case 4:
        _logOut().then((q) {});
        break;
    }
  }

  _onEditProfileClicked() async {
    var data = await Navigator.push(context,
        MaterialPageRoute(builder: (ctx) => EditProfileScreen(meProfile)));
    widget.item.username = data[0];
    widget.item.fullName = data[1];
    setState(() {});
  }

  _onMenuItemSelectedProfile(String name) {
    var i = _menuItems.indexOf(name);
    switch (i) {
      case 0:
        showModalBottomSheet<void>(
            context: this.context,
            builder: (BuildContext context) {
              return new Column(
                  mainAxisSize: MainAxisSize.min,
                  children: _reports.map((name) {
                    return ListTile(
                      onTap: () {
                        saveReport(name);
                        Navigator.pop(context);
                      },
                      leading: SizedBox(),
                      title: Text(name),
                    );
                  }).toList());
            });
        break;
    }
  }

  cancelListTile() {
    ListTile(
      leading: Icon(
        Icons.cancel,
        color: Colors.red,
      ),
      title: Text(
        'Cancel',
        style: TextStyle(color: Colors.red),
      ),
    );
  }

  saveReport(String content) async {
    var act = {
      'ownerId': meProfile.id,
      'user': widget.item.id,
      'description': content,
      'timestamp': FieldValue.serverTimestamp(),
    };
    await Firestore.instance
        .collection(Const.kReportDB)
        .document()
        .setData(act);
    showMessage(context, 'Thank you for your report',
        'Our team will review it in 24 hours.');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('@${widget.item.username}',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          actions: <Widget>[
            PopupMenuButton<String>(
              onSelected:
                  _isMe ? _onMenuItemSelectedMe : _onMenuItemSelectedProfile,
              itemBuilder: (BuildContext context) {
                return _menuItems.map((String name) {
                  return PopupMenuItem<String>(
                    value: name,
                    child: Text(name),
                  );
                }).toList();
              },
            ),
          ],
//          actions: <Widget>[],
        ),
        body: FutureBuilder<List<Post>>(
          future: PostRepository.getUserPosts(widget.item.id),
          builder: (context, snap) {
            var rate = 0;

            if (snap.data != null) {
              snap.data.forEach((post) => rate += post.rating);
              var list = [getHeader(context, rate)];
              list.addAll(
                  snap.data.map((doc) => ProfileActivity(item: doc)).toList());

              return ListView(
                children: list,
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }
}

class ProfileActivity extends StatelessWidget {
  final Post item;

  const ProfileActivity({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: 4.0,
        right: 4.0,
      ),
      child: Card(
        elevation: 2.0,
        child: ListTile(
          contentPadding: EdgeInsets.all(8.0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                toBeginningOfSentenceCase(item.type),
                style: TextStyle(fontSize: 18.0, color: Color(0xFF555555)),
              ),
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Icon(
                    Icons.star,
                    color: Color.fromRGBO(250, 201, 23, 1),
                    size: 32.0,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 2.0),
                    margin: EdgeInsets.all(8.0),
                    child: Text(
                      item.rating.toString(),
                      style: TextStyle(color: Colors.white, fontSize: 8.0),
                    ),
                  )
                ],
              )
            ],
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 32.0, top: 4.0),
                child: Text(
                  item.location,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Color(0xFFADADAD),
                    fontSize: 12.0,
                  ),
                ),
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text(
                      DateFormat('MMM d, yyyy').format(item.timestamp),
                      style: TextStyle(fontSize: 12.0),
                    ),
                  )),
            ],
          ),
          leading: new Container(
            height: 60.0,
            width: 60.0,
            child: new AspectRatio(
              aspectRatio: 487 / 451,
              child: new Container(
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  fit: BoxFit.cover,
                  alignment: FractionalOffset.center,
                  image: new NetworkImage(item.imageUrl ?? ''),
                )),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SelfProfileUser extends StatelessWidget {
  final String userId;

  SelfProfileUser({this.userId});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
      stream: fetchUser,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Scaffold(
            appBar: AppBar(),
            body: Center(child: CircularProgressIndicator()),
          );

        return ProfileUser(item: User.fromDocument(snapshot.data));
      },
    );
  }

  Stream<DocumentSnapshot> get fetchUser async* {
    final user = await FirebaseAuth.instance.currentUser();
    final snap = await Firestore.instance
        .document('user/${user.providerData.last.uid}')
        .get();
    meProfile = User.fromDocument(snap);
    yield* Firestore.instance
        .collection(Const.kUserDB)
        .document(userId ?? user.providerData.last.uid)
        .snapshots();
  }
}

openProfile(BuildContext context, String userId) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SelfProfileUser(
                userId: userId,
              )));
}
