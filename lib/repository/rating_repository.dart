import 'package:Fluttergram/entity/rating.dart';
import 'package:cloud_functions/cloud_functions.dart';

Future<List<RatingFriend>> fetchRating(userId) async {
  var ratings = await CloudFunctions.instance
      .call(functionName: 'getRating', parameters: {'userId': userId}) as List;
  ratings = ratings.map((json) => RatingFriend.fromJson(json)).toList();
  return ratings;
}
