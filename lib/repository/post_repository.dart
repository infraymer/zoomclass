import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/entity/post.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';

Future<List<Post>> fetchPosts(userId) async {
  var posts = await CloudFunctions.instance
      .call(functionName: 'getPosts', parameters: {'userId': userId}) as List;
  posts = posts.map((json) => Post.fromJson(json)).toList();
  return posts;
}

Future<List<Post>> getUserPosts(userId) async {
//    final user = await FirebaseAuth.instance.currentUser();
  var snap = await Firestore.instance
      .collection(Const.kActivitiesDB)
      .where('ownerId', isEqualTo: userId)
      .orderBy('timestamp', descending: true)
      .getDocuments();

  var posts = <Post>[];
  snap.documents.forEach((doc) => posts.add(Post.fromDocument(doc)));

  return posts;
}

Future<void> createPost({
  imageUrl,
  imagePath,
  imageName,
  ownerId,
  ownerUsername,
  ownerDisplayName,
  ownerImage,
  location,
  latitude,
  longitude,
  type,
  typeImage,
  description,
  timestamp,
  rating,
}) async {
  var act = {
    'imageUrl': imageUrl,
    'imagePath': imagePath,
    'imageName': imageName,
    'ownerId': ownerId,
    'ownerUsername': ownerUsername,
    'ownerDisplayName': ownerDisplayName,
    'ownerImage': ownerImage,
    'location': location,
    'latitude': latitude,
    'longitude': longitude,
    'type': type,
    'typeImage': typeImage,
    'description': description,
    'timestamp': timestamp,
    'rating': rating
  };

  await Firestore.instance
      .collection(Const.kActivitiesDB)
      .document()
      .setData(act);
}
