import 'package:Fluttergram/common/const.dart';
import 'package:Fluttergram/entity/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';

String profileId;

Future<List<User>> get fetchFriends2 async {
  final user = await FirebaseAuth.instance.currentUser();

  final profileDoc = await Firestore.instance
      .collection(Const.kUserDB)
      .document(user.providerData.last.uid)
      .get();
  final profile = User.fromDocument(profileDoc);

  var following = List();
  var followers = List();
  following.addAll(profile.following.keys);
  followers.addAll(profile.followers.keys);

  followers.removeWhere((f) => following.contains(f));
  following.addAll(followers);

  var readyUsers = List<User>();

  for (var i = 0; i < following.length; i++) {
    final us = await Firestore.instance
        .collection(Const.kUserDB)
        .document(following[i])
        .get();
    final usr = User.fromDocument(us);
    readyUsers.add(usr);
  }
  return readyUsers;
}

Future<List<User>> fetchFriends(userId) async {
  var users = await CloudFunctions.instance
      .call(functionName: 'getFriends', parameters: {'userId': userId}) as List;
  users = users.map((json) => User.fromJson(json)).toList();
  return users;
}

Future<List<User>> searchUser(String query) async {
  var users = await CloudFunctions.instance
      .call(functionName: 'searchUser', parameters: {'query': query}) as List;
  users = users.map((json) => User.fromJson(json)).toList();
  return users;
}

Future<void> subscribeOnUser(userId, subscribeUserId, bool subscribe) async {
  await Firestore.instance.document("user/$userId").updateData({
    'following.$subscribeUserId':
        subscribe ? FieldValue.serverTimestamp() : FieldValue.delete()
  });
  await Firestore.instance.document("user/$subscribeUserId").updateData({
    'followers.$userId':
        subscribe ? FieldValue.serverTimestamp() : FieldValue.delete()
  });
}

Future<void> updateProfile(
    String userId, String username, String fullname) async {
  await Firestore.instance
      .document("user/$userId")
      .updateData({'username': username, 'displayName': fullname});
}
