import 'package:firebase_storage/firebase_storage.dart';
import 'package:uuid/uuid.dart';

Future<Map> uploadImage(var imageFile) async {
  var uuid = new Uuid().v1();
  StorageReference ref = FirebaseStorage.instance
      .ref()
      .child("activityData/usersActivity/post_$uuid.jpg");
  StorageUploadTask uploadTask = ref.putFile(imageFile);

  String downloadUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
  String path = await (await uploadTask.onComplete).ref.getPath();
  String name = await (await uploadTask.onComplete).ref.getName();
  return {'url': downloadUrl, 'path': path, 'name': name};
}
