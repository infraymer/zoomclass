import 'package:cloud_firestore/cloud_firestore.dart';

class RatingFriend {
  final String id;
  final String username;
  final int sumRating;
  final String avatar;

  const RatingFriend({this.id, this.username, this.sumRating, this.avatar});

  factory RatingFriend.fromDocument(DocumentSnapshot document) {
    return new RatingFriend(
      id: document.documentID,
      username: document['username'],
      sumRating: document['rating'],
      avatar: null,
    );
  }

  factory RatingFriend.fromJson(Map<dynamic, dynamic> document) {
    return new RatingFriend(
      id: document['id'],
      username: document['username'],
      sumRating: document['rating'],
      avatar: document['avatar'],
    );
  }
}
