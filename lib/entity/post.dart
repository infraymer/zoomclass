import 'package:cloud_firestore/cloud_firestore.dart';

class Post {
  final String id;
  final String imageUrl;

  final String ownerId;
  final String ownerUsername;
  final String ownerFullname;
  final String ownerImage;

  final String location;
  final double latitude;
  final double longitude;
  final String type;
  final String typeImage;
  final String description;
  final DateTime timestamp;
  final int rating;

  const Post(
      {this.id,
      this.imageUrl,
      this.ownerId,
      this.ownerUsername,
      this.ownerFullname,
      this.ownerImage,
      this.location,
      this.type,
      this.rating,
      this.description,
      this.timestamp,
      this.latitude,
      this.longitude,
      this.typeImage});

  factory Post.fromDocument(DocumentSnapshot document) {
    return new Post(
      id: document.documentID,
      imageUrl: document['imageUrl'],
      ownerId: document['ownerId'],
      ownerFullname: document['ownerDisplayName'],
      ownerUsername: document['ownerUsername'],
      ownerImage: document['ownerImage'],
      location: document['location'],
      latitude: document['latitude'],
      longitude: document['longitude'],
      type: document['type'],
      typeImage: document['typeImage'],
      rating: document['rating'],
      description: document['description'],
      timestamp: (document['timestamp'] as Timestamp).toDate(),
    );
  }

  factory Post.fromJson(Map<dynamic, dynamic> document) {
    return new Post(
      id: document['id'],
      imageUrl: document['imageUrl'],
      ownerId: document['ownerId'],
      ownerFullname: document['ownerDisplayName'],
      ownerUsername: document['ownerUsername'],
      ownerImage: document['ownerImage'],
      location: document['location'],
      latitude: document['latitude'].toDouble(),
      longitude: document['longitude'].toDouble(),
      type: document['type'],
      typeImage: document['typeImage'],
      rating: document['rating'],
      description: document['description'],
      timestamp: DateTime.fromMillisecondsSinceEpoch(document['timestamp']),
    );
  }
}
