import 'package:cloud_firestore/cloud_firestore.dart';

class Type {
  final String name;
  final int rating;

  Type(this.name, this.rating);
}

class User {
  String id;
  String username;
  String avatar;
  String fullName;
  String location;
  String description;
  Map following;
  Map followers;
  int rating;

  User(
      {this.id,
      this.username,
      this.avatar,
      this.fullName,
      this.location,
      this.description,
      this.rating,
      this.following,
      this.followers});

  factory User.fromDocument(DocumentSnapshot document) {
    return new User(
        id: document.documentID,
        username: document['username'],
        avatar: document['photoUrl'],
        fullName: document['displayName'],
        location: document['location'],
        description: document['description'],
        followers: document['followers'],
        following: document['following']);
  }

  factory User.fromJson(Map<dynamic, dynamic> json) {
    return new User(
        id: json['id'],
        username: json['username'],
        avatar: json['photoUrl'],
        fullName: json['displayName'],
        location: json['location'],
        description: json['description'],
        followers: json['followers'],
        following: json['following']);
  }
}
