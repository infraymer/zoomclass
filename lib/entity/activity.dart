import 'package:cloud_firestore/cloud_firestore.dart';

class Activity {
  final String id;
  final String name;
  final String icon;
  final int rate;

  const Activity({this.id, this.name, this.icon, this.rate});

  factory Activity.fromDocument(DocumentSnapshot document) {
    return new Activity(
      id: document.documentID,
      name: document['name'],
      icon: document['icon'],
      rate: document['rate'],
    );
  }

  factory Activity.fromJSON(Map data) {

    return new Activity(
      id: data['id'],
      name: data['name'],
      icon: data['icon'],
      rate: data['rate'],
    );
  }
}
