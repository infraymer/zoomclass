import 'package:Fluttergram/ui/welcome_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  Firestore.instance.settings(timestampsInSnapshotsEnabled: true).then((_) {
    runApp(new Fluttergram());
  });
}

class Fluttergram extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'Zoomclass',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          textSelectionColor: Colors.black87,
          primaryColor: Color.fromRGBO(244, 244, 244, 1.0),
          primarySwatch: Colors.blue,
          buttonColor: Colors.pink,
          primaryTextTheme: TextTheme(
            title: TextStyle(color: Colors.black87),
          ),
          primaryIconTheme: new IconThemeData(color: Colors.black)),
      home: WelcomePage(),
    );
  }
}
